const jwt = require('jsonwebtoken'),
    secret = require('../config/env.config').jwt_secret;
const { handleError, ErrorHandler}  = require('../handlers/errorHandler');


    const auth = async(req, res, next)=>{
        const token = req.header('Authorization');
        if(!token) return res.status(401).json({message:'No authorization'})
        try {
         const payload = jwt.verify(token,secret);
         req.payload=payload;
         next();
        } catch (error) {
        //console.log(error);
        res.status(403).json({message:'Invalid Token'});
        }
    };

module.exports={auth}