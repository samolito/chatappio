const Chatroom = require('../model/Chatroom');
let  { handleError, ErrorHandler}  = require('../handlers/errorHandler');

const create= async (req,res, next)=>{

    try {
        let chatroom = new Chatroom(req.body);
        if( await Chatroom.findOne({name:chatroom.name})){
            throw new ErrorHandler(201,"Chatroom with that name already exists!",null);
        }

        await chatroom.save();
        res.status(201).json({
             status:1, 
             message: ` Chatroom ${chatroom.name} created successfully`,
             chatroom:chatroom
             });
             
        } catch (error) {
        next(error)
    }
    };
    const getChatroom= async (req,res, next)=>{

        try {
            let chatrooms=await Chatroom.find()
            res.status(201).json({
                 status:1, 
                 chatrooms
                 });
            } catch (error) {
            next(error)
        }
        };
        const getoneChatroom= async (req,res, next)=>{

           let chatroom=await Chatroom.findOne({name:req.body.name})
            try {
                if(!chatroom){
                    throw new ErrorHandler(201,`Chatroom not found`,null);
                }
                res.status(201).json({
                     status:1, 
                     chatroom:chatroom
                     });
                } catch (error) {
                next(error)
            }
            };
module.exports={create,getChatroom,getoneChatroom}