const User = require('../model/User')
jwtSecret = require('../config/env.config').jwt_secret
const jwt = require ('jsonwebtoken');
const bycrypt= require('bcryptjs')
let  { handleError, ErrorHandler}  = require('../handlers/errorHandler');


const register= async (req,res, next)=>{
try {
    let user = new User(req.body);
        if(await User.findOne({email:user.email})){
            throw new ErrorHandler(201,'User already exists',null);
        }
        //Generate Salt
        const salt = await bycrypt.genSalt(10);
        //Hash user password
        user.password = await bycrypt.hash(req.body.password,salt);
    //save user
    await  user.save();
    //Create payload and 
    const payload = { user: { id: user.id,username:user.username, email:user.email,username:user.password} };
    //Generate Token
    let token = jwt.sign(payload,jwtSecret);
    res.status(201).json({ status:1, message: "User created successfully", token });
    next()
    } catch (error) {
    next(error)
}
}

const login= async (req,res, next)=>{
    const {email, password } = req.body;
    try {
        const user = await User.findOne({email})
        if(!user){
            throw new ErrorHandler(201,"User doesn't exists",null);
        }
        if (!await bycrypt.compare(password, user.password))
        {
            throw new ErrorHandler(401,'Incorrect Password',null);
            
        }
        //Create payload and 
        const payload = { user: { id: user.id,username:user.username, email:user.email,username:user.password} };
        //Generate Token
        let token = jwt.sign(payload,jwtSecret);
        res.status(201).json({ status:1, message: "User login successfully", token });
        next()
        } catch (error) {
        next(error)
    }
    };
    const getUsers= async (req,res, next)=>{

        try {
            let users=await User.find()
            res.status(201).json({
                 status:1, 
                 users
                 });
            } catch (error) {
            next(error)
        }
        };
module.exports={register,login, getUsers}