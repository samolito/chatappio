const mongoose = require('mongoose');

const ChatRoomShema = mongoose.Schema({
        name:{type:String, required:"name is required"},
        createdOn : {type: Date, default: Date.now}
});
module.exports=mongoose.model('chatroom',ChatRoomShema);