const mongoose = require('mongoose');

const MessageSchema= mongoose.Schema({

    chatroom:{type:mongoose.Schema.Types.ObjectId, required:"Chatroom is required", ref:"chatroom"},
    users:[{type:mongoose.Schema.Types.ObjectId,required:"User is required", ref:"user"}],
    message:{type:String},
    createdOn : {type: Date, default: Date.now}
});

module.exports=mongoose.model('message', MessageSchema);