
const mongoose=require('mongoose');

const UserSchema = mongoose.Schema({
    username: { type:String,        required: true },
    email:    { type:String,        required: true },
    password: { type:String,        required: true  },
    status:   { type:Boolean,       required: true, default:true }
},
{ 
    timestamps: true,
  })

module.exports=mongoose.model('user',UserSchema);