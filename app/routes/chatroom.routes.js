const router =require('express').Router();
const chatroomController= require('../controllers/chatroom.controller');
const auth = require('../auth/auth').auth


router.post('/create',chatroomController.create);
router.get('/getchatrooms',chatroomController.getChatroom);
router.get('/getone/:name',chatroomController.getoneChatroom);
module.exports=router;