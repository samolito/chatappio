const router =require('express').Router();
const userController= require('../controllers/user.controller');
const auth = require('../auth/auth').auth


router.post('/register',userController.register);
router.post('/login', userController.login);
router.get('/getall', userController.getUsers);
module.exports=router;