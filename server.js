const express = require('express'),
bodyParser = require('body-parser'),
config =require('./app/config/env.config'),
cors = require('cors');
MongoServer=require('./app/db/db.config'),
user_route = require('./app/routes/user.routes'),
charoom_route= require('./app/routes/chatroom.routes')
handleError = require('./app/handlers/errorHandler').handleError,
app = express(),
http=require('http').Server(app)
io=require('socket.io')(http)
const jwt = require("jwt-then");
const secret = require('./app/config/env.config').jwt_secret;
//Call mongo DB server
MongoServer()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors())
app.use('/api/user',user_route);
app.use('/chatroom',charoom_route);

app.use('/',(req,res)=>{
    res.sendFile(__dirname+'/app/views/index.html')
})
//Handler Error
app.get('error',(req, res)=>{
    throw new ErrorHandler(500,'Internal Server Error')
});
app.use((err, req, res, next) => {
    handleError(err, res);
  });

  
  io.on('connection',function(socket){
    console.log('A user is connected');

    socket.on('disconnect',function(){
        console.log('User disconnected')
    })
    socket.on('chat message',function(msg){
        console.log('Message recu :'+msg)
        io.emit('chat message', msg);
    })
})

 // Start Server
http.listen(config.port,()=>{
    console.log("server is connected on port :"+config.port)
});

const Message = require('./app/model/Message');
const User= require('./app/model/User');
const Chatroom= require('./app/model/Chatroom');
// //VeryfyUserTOken






